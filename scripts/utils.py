def norm_dist_map(orig_file, dest_file):
    """compute normalized distance map given an origin and destination masks, resp."""
    import os
    import nibabel as nib
    import numpy as np
    from scipy.ndimage.morphology import distance_transform_edt

    orig_nib = nib.load(orig_file)
    dest_nib = nib.load(dest_file)

    orig = orig_nib.get_fdata()
    dest = dest_nib.get_fdata()

    dist_orig = distance_transform_edt(np.logical_not(orig.astype(bool)))
    dist_dest = distance_transform_edt(np.logical_not(dest.astype(bool)))

    # normalized distance (0 in origin to 1 in dest)
    ndist = dist_orig / (dist_orig + dist_dest)

    ndist_nib = nib.Nifti1Image(ndist.astype(np.float32), orig_nib.affine)
    nib.save(ndist_nib, 'ndist.nii.gz')

    return os.path.abspath('ndist.nii.gz')
