# Generate a regional-zonal parcellation of WM for investigating WMH spatial distribution

This is based on the original paper proposing a regional-zonal analysis of WMH [Sudre et al. 2019](https://www.sciencedirect.com/science/article/pii/S0150986117302249#bib0140). The algorithm described for deriving the bullseye representation in subject space by [Jimenez-Balado](10.1038/s41598-022-06019-8) was adapted for MNI152 space.

1. Creation of four layer equally spaced distance map from ventricles to cortex

### Perform segmentation of MNI152_T1_1mm image (included in FSL) using fast
FSL --version 5.0.11
fast /data/pt_life_whm/Data/segmented_MNI1mm/MNI152_T1_1mm_brain.nii.gz

### threshold gray matter probability map at 0.9
fslmaths /data/pt_life_whm/Data/segmented_MNI1mm/MNI152_T1_1mm_brain_pve_1.nii.gz -thr 0.9 -bin /data/pt_life_whm/Data/segmented_MNI1mm/MNI152_T1_1mm_brain_pve_1_thr0.9_bin.nii.gz

### derive cortical labels from HarvardOxford atlas & mask GMV map with it (to exclude subcortical volumes)
fslmaths /data/pt_life_whm/Data/segmented_MNI1mm/harvardoxford-cortical_label_all.nii -bin /data/pt_life_whm/Data/segmented_MNI1mm/harvardoxford-cortical_label_bin.nii
fslmaths /data/pt_life_whm/Data/segmented_MNI1mm/MNI152_T1_1mm_brain_pve_1_thr0.9_bin.nii.gz -mas harvardoxford-cortical_label_bin.nii /data/pt_life_whm/Data/segmented_MNI1mm/MNI152_T1_1mm_brain_pve_1_cortlabelsonly_thr0.9_bin.nii.gz

## calculate distancemap from cortex
distancemap-i /data/pt_life_whm/Data/segmented_MNI1mm/MNI152_T1_1mm_brain_pve_1_cortlabelsonly_thr0.9_bin.nii.gz -o /data/pt_life_whm/Data/segmented_MNI1mm/MNI152_T1_1mm_brain_pve_1_cortlabelsonly_thr0.9_bin_distancemap.nii.gz

## create lateral ventricle map thresholded at 0.2
fslmaths /data/pt_life_whm/Data/segmented_MNI1mm/harvardoxford-subcortical_prob_RightLateralVentricle.nii -add /data/pt_life_whm/Data/segmented_MNI1mm/harvardoxford-subcortical_prob_LeftLateralVentricle.nii -thr 20 -bin harvardoxford-subcortical_prob_LateralVentricle_thr0.2_bin.nii

## calculate distancemap from ventricle
distancemap -i /data/pt_life_whm/Data/segmented_MNI1mm/harvardoxford-subcortical_prob_LateralVentricle_thr0.2_bin.nii -o /data/pt_life_whm/Data/segmented_MNI1mm/harvardoxford-subcortical_prob_LateralVentricle_thr0.2_bin_distancemap.nii.gz

## Sum the distance maps and create relative distance map according to Jimenez-Balado
fslmaths MNI152_T1_1mm_brain_pve_1_cortlabelsonly_thr0.9_bin_distancemap.nii.gz -add harvardoxford-subcortical_prob_LateralVentricle_thr0.2_bin_distancemap.nii.gz sum_distancemaps.nii.gz
fslmaths harvardoxford-subcortical_prob_LateralVentricle_thr0.2_bin_distancemap.nii.gz -div sum_distancemaps rel_distancemap_ventricle_cortex.nii.gz

# Create WM probability maps
fslmaths MNI152_T1_1mm_brain_pve_2.nii.gz -bin MNI152_T1_1mm_brain_pve_2_bin.nii.gz

# binarize and threshold more outer layers with WM probability map
fslmaths rel_distancemap_ventricle_cortex.nii.gz -thr 0 -uthr 0.25 -bin rel_distancemap_ventricle_cortex_0_0.25.nii.gz
fslmaths rel_distancemap_ventricle_cortex.nii.gz -thr 0.25 -uthr 0.5 -bin rel_distancemap_ventricle_cortex_0.25_0.5.nii.gz
fslmaths rel_distancemap_ventricle_cortex.nii.gz -thr 0.501 -uthr 0.75 -mas MNI152_T1_1mm_brain_pve_2_bin.nii.gz -bin rel_distancemap_ventricle_cortex_0.5_0.75.nii.gz
fslmaths rel_distancemap_ventricle_cortex.nii.gz -thr 0.7501 -uthr 1 -mas MNI152_T1_1mm_brain_pve_2_bin.nii.gz -bin rel_distancemap_ventricle_cortex_0.75_1.nii.gz


2. create lobar masks in MNI space:

## define different lobes based on [Freesurfer's aparc parcellation](https://surfer.nmr.mgh.harvard.edu/fswiki/CorticalParcellation)

1. first run: ./test_annotlabels.sh to split labels from aparc.annot and re-merge into four lobes (inspired by: https://www.mail-archive.com/freesurfer@nmr.mgh.harvard.edu/msg59005.html)
2. do "mris_label2annot" to create another annotation based on own colormap
mris_label2annot --s fsaverage --h rh --l fsaverage/labels_new/lobes/rh.frontal --l fsaverage/labels_new/lobes/rh.temporal --l fsaverage/labels_new/lobes/rh.parietal --l fsaverage/labels_new/lobes/rh.occipital --a myannot --ctab fsaverage/labels_new/myctab.ctab (for both hemispheres)
3. run "mri_aparc2aseg --s fsaverage --labelwm --hypo-as-wm --rip-unknown \
   --volmask --wmparc-dmax 20 --o wmparc.mylobesonly.mgz \
   --annot myannot --base-offset 200"
to create labeled volume file


## register lobar segmentation to MNI152_T1_1mm_brain
mri_label2vol --reg $FREESURFER_HOME/average/mni152.register.dat --seg wmparc.mylobesonly.mgz --temp $FSLDIR/data/standard/MNI152_T1_1mm.nii.gz --o /data/pt_life_whm/Data/segmented_MNI1mm/wmparc.mylobesonly.mni152.nii


## combine the four distancemaps & lobar maps
fslmaths wmparc.lobesonly.mni152.nii -mas rel_distancemap_ventricle_cortex_0_0.25.nii.gz /data/pt_life_whm/Data/segmented_MNI1mm/wmparc.lobesonly.mni152_1shell.nii
fslmaths wmparc.lobesonly.mni152.nii -mas rel_distancemap_ventricle_cortex_0.25_0.5.nii.gz /data/pt_life_whm/Data/segmented_MNI1mm/wmparc.lobesonly.mni152_2shell.nii
fslmaths wmparc.lobesonly.mni152.nii -mas rel_distancemap_ventricle_cortex_0.5_0.75.nii.gz /data/pt_life_whm/Data/segmented_MNI1mm/wmparc.lobesonly.mni152_3shell.nii
fslmaths wmparc.lobesonly.mni152.nii -mas rel_distancemap_ventricle_cortex_0.75_1.nii.gz /data/pt_life_whm/Data/segmented_MNI1mm/wmparc.lobesonly.mni152_4shell.nii


### change the label number for a specific roi & create atlas by running the script
./create_atlas.sh

### based on the following labels:
3201  & 4201 ->  frontal-lobe
3202 & 4202 -> temporal lobe
3203 & 4203 -> parietal lobe
3204 & 4204 ->  occipital lobe

### Basal Ganglia (BG) was labeled considering caudate, putamen, thalamus and globus pallidus subcortical segmentations:
43  Right-Lateral-Ventricle 
48  Right-Thalamus                          0   118 14  0
49  Right-Thalamus-Proper*                  0   118 14  0
50  Right-Caudate                           122 186 220 0
51  Right-Putamen                           236 13  176 0
52  Right-Pallidum                          13  48  255 0

4   Left-Lateral-Ventricle                  120 18  134 0
9   Left-Thalamus                           0   118 14  0
10  Left-Thalamus-Proper*                   0   118 14  0
11  Left-Caudate                            122 186 220 0
12  Left-Putamen                            236 13  176 0
13  Left-Pallidum                           12  48  255 0

## bullseye MNI atlas and lookuptable
/data/pt_life_whm/Data/segmented_MNI1mm/UKBB_spatialWMH/bullseye_MNIatlas_1mm.nii.gz
/data/pt_life_whm/Data/segmented_MNI1mm/UKBB_spatialWMH/lookuptable_bullseye_MNIatlas.txt


## Previously FOR CREATING LOBAR ANNOTATION  (DEPRECATED NOW) 
# based on freesurfer-internal lobar annotation (which also creates insula + cingulate separately, so not ideal)
# Below is the color table for a lobar white matter parcellation
#  obtained from running:
# mri_annotation2label --subject subject --hemi lh --lobesStrict lobes
# mri_annotation2label --subject subject --hemi rh --lobesStrict lobes
# mri_aparc2aseg --s subject --labelwm --hypo-as-wm --rip-unknown \
#   --volmask --o wmparc.lobes.mgz --ctxseg aparc+aseg.mgz \
#   --annot lobes --base-offset 200 [--base-offset must be last arg]

3201    wm-lh-frontal-lobe                  235 35  95  0
3203    wm-lh-cingulate-lobe                35  75  35  0
3204    wm-lh-occipital-lobe                135 155 195 0
3205    wm-lh-temporal-lobe                 115 35  35  0
3206    wm-lh-parietal-lobe                 35  195 35  0
3207    wm-lh-insula-lobe                   20  220 160 0

4201    wm-rh-frontal-lobe                  235 35  95  0
4203    wm-rh-cingulate-lobe                35  75  35  0
4204    wm-rh-occipital-lobe                135 155 195 0
4205    wm-rh-temporal-lobe                 115 35  35  0
4206    wm-rh-parietal-lobe                 35  195 35  0
4207    wm-rh-insula-lobe                   20  220 160 0

mri_annotation2label --subject fsaverage --hemi lh --lobesStrict lobes
mri_annotation2label --subject fsaverage --hemi rh --lobesStrict lobes
mri_aparc2aseg --s fsaverage --labelwm --hypo-as-wm --rip-unknown \
   --volmask --wmparc-dmax 20 --o wmparc.mylobesonly.mgz \
   --annot mylobe --base-offset 200

   
