#!/bin/bash
# Extract spatial WMH load

# Merged input file (WMH maps transformed into MNI space and binarized at 0.1 ? ) 
input="/path/to/mergedfile.nii.gz"

MNImask="../UKBB_spatialWMH/bullseye_MNIatlas_1mm.nii.gz"

fslstats -t -K $MNImask $input -V >> res_bullseye_WMH.txt


# If single WMH maps are available (WMH maps transformed into MNI space and binarized at 0.1 ? )
for WMHmap in /some/path/to/lesion/*bianca*.nii.gz
do

echo $WMHmap >> inputfiles.txt
fslstats -K $MNImask $WMHmap -V >> res_bullseye_WMH_MNI.txt

done

