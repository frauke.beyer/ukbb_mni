# Generate a regional-zonal parcellation of WM for investigating WMH spatial distribution

This is based on the original paper proposing a regional-zonal analysis of WMH [Sudre et al. 2019](https://www.sciencedirect.com/science/article/pii/S0150986117302249#bib0140). The lobar segmentation was taken from the Hammers atlas [Jimenez-Balado](10.1038/s41598-022-06019-8) was adapted for MNI152 space.

1. Creation of four layer equally spaced distance map from ventricles to cortex

cd /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/

### Perform segmentation of MNI152_T1_1mm image (included in FSL) using fast
FSL --version 5.0.11
fast /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/original_segmented_MNI//MNI152_T1_1mm_brain.nii.gz

### threshold gray matter probability map at 0.9
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/original_segmented_MNI//MNI152_T1_1mm_brain_pve_1.nii.gz -thr 0.9 -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/original_segmented_MNI//MNI152_T1_1mm_brain_pve_1_thr0.9_bin.nii.gz

### derive cortical labels from HarvardOxford atlas & mask GMV map with it (to exclude subcortical volumes)
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI//harvardoxford-cortical_label_all.nii -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/harvardoxford-cortical_label_bin.nii
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/original_segmented_MNI/MNI152_T1_1mm_brain_pve_1_thr0.9_bin.nii.gz -mas harvardoxford-cortical_label_bin.nii /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/MNI152_T1_1mm_brain_pve_1_cortlabelsonly_thr0.9_bin.nii.gz

## calculate distancemap from cortex -> not done because slow
#distancemap-i /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/MNI152_T1_1mm_brain_pve_1_cortlabelsonly_thr0.9_bin.nii.gz -o /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/distance_maps/MNI152_T1_1mm_brain_pve_1_cortlabelsonly_thr0.9_bin_distancemap.nii.gz

## create lateral ventricle map thresholded at 0.2 (first version) or 0.5 (second version) OR from HAMMERS (best agreement with lobar segmentation)
#fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/harvardoxford-subcortical_prob_RightLateralVentricle.nii -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI//harvardoxford-subcortical_prob_LeftLateralVentricle.nii -thr 50 -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/distance_maps/harvardoxford-subcortical_prob_LateralVentricle_thr0.5_bin.nii
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_45.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_46.nii.gz /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_ventricles.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_ventricles.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_47.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_ventricles.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_ventricles.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_48.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_ventricles.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_ventricles.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_49.nii.gz /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_ventricles.nii.gz

flirt -in /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_ventricles.nii.gz -ref /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/original_segmented_MNI/MNI152_T1_1mm_brain.nii.gz -usesqform -applyxfm -out /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_ventricles.nii.gz


## calculate distancemap from ventricle > not done because slow
#distancemap -i /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/distance_maps/harvardoxford-subcortical_prob_LateralVentricle_thr0.5_bin.nii.gz -o /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/distance_maps/harvardoxford-subcortical_prob_LateralVentricle_thr0.5_bin_distancemap.nii.gz

## Sum the distance maps and create relative distance map according to Jimenez-Balado
#fslmaths distance_maps/MNI152_T1_1mm_brain_pve_1_cortlabelsonly_thr0.9_bin_distancemap.nii.gz -add distance_maps/harvardoxford-subcortical_prob_LateralVentricle_thr0.5_bin_distancemap.nii.gz distance_maps/sum_distancemaps_thr0.5.nii.gz
#fslmaths distance_maps/harvardoxford-subcortical_prob_LateralVentricle_thr0.5_bin_distancemap.nii.gz -div distance_maps/sum_distancemaps distance_maps/rel_distancemap_ventricle_thr0.5_cortex.nii.gz

## Calculate normalized distance maps in python
miniconda
FREESURFER
source activate agewell_nip1.2
import nipype.pipeline.engine as pe
from nipype import SelectFiles
import nipype.interfaces.utility as util

from nipype import IdentityInterface, DataSink

# from .utils import *
from utils import *

import os
ndist_map = pe.Node(interface=util.Function(input_names=['orig_file', 'dest_file'], output_names=['rel_distancemap_ventricle_edited_thr0.5_cortex.nii.gz'],
                                                function=norm_dist_map), name='ndist_map')
ndist_map.inputs.orig_file="/data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_ventricles_edited.nii.gz"                                                
ndist_map.inputs.dest_file="/data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/distance_maps/MNI152_T1_1mm_brain_pve_1_cortlabelsonly_thr0.9_bin.nii.gz" 
ndist_map.run()

cd /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/distance_maps/Hammers_edited/
distmap="rel_distancemap_ventricle_edited_thr0.5_cortex.nii.gz"


# binarize and threshold more outer layers with WM probability map
fslmaths $distmap -thr 0 -uthr 0.25 -bin rel_distancemap_ventricle_thr0.5_cortex_0_0.25.nii.gz
fslmaths $distmap -thr 0.2501 -uthr 0.5 -bin rel_distancemap_ventricle_thr0.5_cortex_0.25_0.5.nii.gz
fslmaths $distmap -thr 0.501 -uthr 0.75 -mas MNI152_T1_1mm_brain_pve_2_bin.nii.gz -bin rel_distancemap_ventricle_thr0.5_cortex_0.5_0.75.nii.gz
fslmaths $distmap -thr 0.7501 -uthr 1 -mas MNI152_T1_1mm_brain_pve_2_bin.nii.gz -bin rel_distancemap_ventricle_thr0.5_cortex_0.75_1.nii.gz

#fslmaths $distmap -thr 0 -uthr 0.33 -bin rel_distancemap_ventricle_thr0.5_cortex_0_0.33.nii.gz
#fslmaths $distmap -thr 0.3301 -uthr 0.66 -bin rel_distancemap_ventricle_thr0.5_cortex_0.33_0.66.nii.gz
#fslmaths $distmap -thr 0.6601 -uthr 1 -mas MNI152_T1_1mm_brain_pve_2_bin.nii.gz -bin rel_distancemap_ventricle_thr0.5_cortex_0.66_1.nii.gz


# Create WM probability maps
#fslmaths MNI152_T1_1mm_brain_pve_2.nii.gz -bin MNI152_T1_1mm_brain_pve_2_bin.nii.gz


2. create lobar masks in MNI space from Hammers atlas (https://pubmed.ncbi.nlm.nih.gov/12874777/) & (https://www.biorxiv.org/content/10.1101/2023.01.20.524929v1.full.pdf)
## Odd numbers always indicate a region on the right side of the brain

#generate all single ROIs

for i in {82..83} #5
do
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_mith_atlas_n30r83_SPM5.nii.gz -uthr ${i} -thr ${i}  /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_${i}.nii.gz
done


##CC (use y Coordinate of cingulate cortex for division)
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_mith_atlas_n30r83_SPM5.nii.gz -thr 44 -uthr 44 /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_CC.nii.gz 
fslroi /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_CC.nii.gz /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_CC_ant_rh.nii.gz 0 90 123 217 0 -1 
flirt -in /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_CC_ant_rh.nii.gz -ref /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_CC.nii.gz -usesqform -applyxfm -out /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_CC_ant_rh.nii.gz
fslroi /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_CC.nii.gz /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_CC_ant_lh.nii.gz 90 92 123 217 0 -1 
flirt -in /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_CC_ant_lh.nii.gz -ref /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_CC.nii.gz -usesqform -applyxfm -out /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_CC_ant_lh.nii.gz


fslroi /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_CC.nii.gz  /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_CC_post_rh.nii.gz 0 90 0 123 0 -1 
flirt -in /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_CC_post_rh.nii.gz -ref /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_CC.nii.gz -usesqform -applyxfm -out /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_CC_post_rh.nii.gz
fslroi /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_CC.nii.gz  /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_CC_post_lh.nii.gz 90 92 0 123 0 -1 
flirt -in /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_CC_post_lh.nii.gz -ref /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_CC.nii.gz -usesqform -applyxfm -out /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_CC_post_lh.nii.gz

##frontal 
28; 29 Middle frontal gyrus
50; 51 Precentral gyrus
52; 53 Straight gyrus
54; 55 Anterior orbital gyrus
56; 57 Inferior frontal gyrus
58; 59 Superior frontal gyrus
68; 69 Medial orbital gyrus
70; 71 Lateral orbital gyrus
72; 73 Posterior orbital gyrus
76; 77 Subgenual frontal cortex
78; 79 Subcallosal area
80; 81 Pre-subgenual frontal cortex
# + 
24; 25 cingulate gyrus (frontal part)
#+ insula (as in Bullseye)
20;21


fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_28.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_50.nii.gz /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_lh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_lh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_20.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_lh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_lh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_24.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_lh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_lh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_52.nii.gz /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_lh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_lh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_54.nii.gz /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_lh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_lh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_56.nii.gz /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_lh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_lh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_58.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_lh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_lh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_68.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_lh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_lh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_70.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_lh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_lh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_72.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_lh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_lh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_76.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_lh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_lh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_78.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_lh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_lh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_80.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_lh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_lh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_CC_ant_lh.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_lh.nii.gz

flirt -in /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_lh.nii.gz -ref /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/original_segmented_MNI/MNI152_T1_1mm_brain.nii.gz -usesqform -applyxfm -out /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_lh2MNI.nii.gz


fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_29.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_51.nii.gz /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_rh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_rh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_21.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_rh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_rh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_25.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_rh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_rh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_53.nii.gz /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_rh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_rh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_55.nii.gz /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_rh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_rh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_57.nii.gz /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_rh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_rh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_59.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_rh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_rh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_69.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_rh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_rh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_71.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_rh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_rh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_73.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_rh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_rh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_77.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_rh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_rh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_79.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_rh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_rh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_81.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_rh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_rh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_CC_ant_rh.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_rh.nii.gz

flirt -in /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_rh.nii.gz -ref /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/original_segmented_MNI/MNI152_T1_1mm_brain.nii.gz -usesqform -applyxfm -out /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_frontal_rh2MNI.nii.gz


#temporal 
##1; 2 Hippocampus
##3; 4 Amygdala
5; 6 Anterior temporal lobe, medial part
7; 8 Anterior temporal lobe, lateral part
9; 10 Parahippocampal and ambient gyri
11; 12 Superior temporal gyrus, posterior part
13; 14 Middle and inferior temporal gyrus
15; 16 Fusiform gyrus
30; 31 Posterior temporal lobe
82; 83 Superior temporal gyrus, anterior part

fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_6.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_8.nii.gz /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_temporal_lh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_temporal_lh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_10.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_temporal_lh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_temporal_lh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_12.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_temporal_lh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_temporal_lh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_14.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_temporal_lh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_temporal_lh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_16.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_temporal_lh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_temporal_lh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_30.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_temporal_lh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_temporal_lh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_82.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_temporal_lh.nii.gz

#bring to 182x218x182 space
flirt -in /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_temporal_lh.nii.gz -ref /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/original_segmented_MNI/MNI152_T1_1mm_brain.nii.gz -usesqform -applyxfm -out /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_temporal_lh2MNI.nii.gz

#right hemisphere
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_5.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_7.nii.gz /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_temporal_rh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_temporal_rh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_9.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_temporal_rh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_temporal_rh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_11.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_temporal_rh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_temporal_rh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_13.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_temporal_rh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_temporal_rh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_15.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_temporal_rh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_temporal_rh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_31.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_temporal_rh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_temporal_rh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_83.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_temporal_rh.nii.gz

#bring to 182x218x182 space
flirt -in /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_temporal_rh.nii.gz -ref /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/original_segmented_MNI/MNI152_T1_1mm_brain.nii.gz -usesqform -applyxfm -out /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_temporal_rh2MNI.nii.gz


#Occipital Lobe
64; 65 Lingual gyrus
66; 67 Cuneus
22; 23 Lateral remainder of occipital lobe

fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_64.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_66.nii.gz /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_occipital_lh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_occipital_lh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_22.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_occipital_lh.nii.gz

#bring to 182x218x182 space
flirt -in /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_occipital_lh.nii.gz -ref /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/original_segmented_MNI/MNI152_T1_1mm_brain.nii.gz -usesqform -applyxfm -out /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_occipital_lh2MNI.nii.gz

#right hemisphere
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_65.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_67.nii.gz /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_occipital_rh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_occipital_rh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_23.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_occipital_rh.nii.gz

#bring to 182x218x182 space
flirt -in /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_occipital_rh.nii.gz -ref /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/original_segmented_MNI/MNI152_T1_1mm_brain.nii.gz -usesqform -applyxfm -out /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_occipital_rh2MNI.nii.gz


##Parietal Lobe
60; 61 Postcentral gyrus
62; 63 Superior parietal gyrus
32; 33 Inferiolateral remainder of parietal lobe


fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_60.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_62.nii.gz /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_parietal_lh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_parietal_lh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_32.nii.gz /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_parietal_lh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_parietal_lh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_26.nii.gz /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_parietal_lh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_parietal_lh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_CC_post_lh.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_parietal_lh.nii.gz

#bring to 182x218x182 space
flirt -in /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_parietal_lh.nii.gz -ref /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/original_segmented_MNI/MNI152_T1_1mm_brain.nii.gz -usesqform -applyxfm -out /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_parietal_lh2MNI.nii.gz

#right hemisphere
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_61.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_63.nii.gz /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_parietal_rh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_parietal_rh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_33.nii.gz /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_parietal_rh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_parietal_rh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_27.nii.gz /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_parietal_rh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_parietal_rh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_CC_post_rh.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_parietal_rh.nii.gz

#bring to 182x218x182 space
flirt -in /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_parietal_rh.nii.gz -ref /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/original_segmented_MNI/MNI152_T1_1mm_brain.nii.gz -usesqform -applyxfm -out /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_parietal_rh2MNI.nii.gz


#Basal ganglia -> same as for FreeSurfer version
34; 35 Caudate nucleus
36; 37 Nucleus accumbens
38; 39 Putamen
40; 41 Thalamus
42; 43 Pallidum

fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_34.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_36.nii.gz /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_BG_lh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_BG_lh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_38.nii.gz /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_BG_lh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_BG_lh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_40.nii.gz /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_BG_lh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_BG_lh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_42.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_BG_lh.nii.gz

flirt -in /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_BG_lh.nii.gz -ref /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/original_segmented_MNI/MNI152_T1_1mm_brain.nii.gz -usesqform -applyxfm -out /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_BG_lh2MNI.nii.gz


fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_35.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_37.nii.gz /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_BG_rh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_BG_rh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_39.nii.gz /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_BG_rh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_BG_rh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_41.nii.gz /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_BG_rh.nii.gz
fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_BG_rh.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_43.nii.gz -bin /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_BG_rh.nii.gz
#bring to 182x218x182 space
flirt -in /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_BG_rh.nii.gz -ref /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/original_segmented_MNI/MNI152_T1_1mm_brain.nii.gz -usesqform -applyxfm -out /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_BG_rh2MNI.nii.gz

fslmaths /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_BG_rh2MNI.nii.gz -add /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_BG_lh2MNI.nii.gz /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers/Hammers_BG2MNI.nii.gz


### change the label number for a specific roi & create atlas by running the script
./create_atlas_hammers.sh


## bullseye MNI atlas and lookuptable
/data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/segmented_MNI1mm/UKBB_spatialWMH/bullseye_MNIatlas_1mm.nii.gz
/data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/segmented_MNI1mm/UKBB_spatialWMH/lookuptable_bullseye_MNIatlas.txt


