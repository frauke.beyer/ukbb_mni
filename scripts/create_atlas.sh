#!/bin/bash
cd /data/pt_life_whm/Data/segmented_MNI1mm/individual_regions

lobesonly="/data/pt_life_whm/Data/segmented_MNI1mm/wmparc.mylobesonly.mni152.nii"

input="/data/pt_life_whm/Data/segmented_MNI1mm/shell_masked_lobes/wmparc.mylobesonly.mni152"
output="wmparc.mylobesonly.mni152"


fslmaths $lobesonly -mas /data/pt_life_whm/Data/segmented_MNI1mm/distance_maps/rel_distancemap_ventricle_cortex_0_0.25.nii.gz /data/pt_life_whm/Data/segmented_MNI1mm/shell_masked_lobes/${output}_1shell.nii
fslmaths $lobesonly -mas /data/pt_life_whm/Data/segmented_MNI1mm/distance_maps/rel_distancemap_ventricle_cortex_0.25_0.5.nii.gz /data/pt_life_whm/Data/segmented_MNI1mm/shell_masked_lobes/${output}_2shell.nii
fslmaths $lobesonly -mas /data/pt_life_whm/Data/segmented_MNI1mm/distance_maps/rel_distancemap_ventricle_cortex_0.5_0.75.nii.gz /data/pt_life_whm/Data/segmented_MNI1mm/shell_masked_lobes/${output}_3shell.nii
fslmaths $lobesonly -mas /data/pt_life_whm/Data/segmented_MNI1mm/distance_maps/rel_distancemap_ventricle_cortex_0.75_1.nii.gz /data/pt_life_whm/Data/segmented_MNI1mm/shell_masked_lobes/${output}_4shell.nii



for shell in 1shell 2shell 3shell 4shell
do




for hemi in lh rh
do
if [ $hemi == "lh" ]; 
then
fslmaths ${input}_$shell.nii -thr 9 -uthr 13 -bin ${output}_${shell}_bg_$hemi.nii -odt double
fslmaths ${input}_$shell.nii -thr 3201 -uthr 3201 -bin ${output}_${shell}_frontal_$hemi.nii -odt double
fslmaths ${input}_$shell.nii -thr 3202 -uthr 3202 -bin ${output}_${shell}_temporal_$hemi.nii -odt double
fslmaths ${input}_$shell.nii -thr 3203 -uthr 3203 -bin ${output}_${shell}_parietal_$hemi.nii -odt double
fslmaths ${input}_$shell.nii -thr 3204 -uthr 3204 -bin ${output}_${shell}_occipital_$hemi.nii -odt double


else
fslmaths ${input}_$shell.nii -thr 48 -uthr 52 -bin ${output}_${shell}_bg_$hemi.nii -odt double
fslmaths ${input}_$shell.nii -thr 4201 -uthr 4201 -bin ${output}_${shell}_frontal_$hemi.nii -odt double
fslmaths ${input}_$shell.nii -thr 4202 -uthr 4202 -bin ${output}_${shell}_temporal_$hemi.nii -odt double
fslmaths ${input}_$shell.nii -thr 4203 -uthr 4203 -bin ${output}_${shell}_parietal_$hemi.nii -odt double
fslmaths ${input}_$shell.nii -thr 4204 -uthr 4204 -bin ${output}_${shell}_occipital_$hemi.nii -odt double



fi

done

fslmaths ${output}_${shell}_bg_lh.nii.gz -add ${output}_${shell}_bg_rh.nii ${output}_${shell}_bg.nii.gz
rm -rf ${output}_*_bg_rh.nii.gz
rm -rf ${output}_*_bg_lh.nii.gz

done



# Create atlas file (1-36)
count=1

for file in wmparc.mylobes*
do
echo $file >> order.txt

if [ $count == 1 ];
then
echo "creating first tmp"
fslmaths $file -mul $count bullseyeWMparc_MNIspace_1mm.nii.gz
else
echo "adding other volumes first tmp"
fslmaths $file -mul $count -add bullseyeWMparc_MNIspace_1mm.nii.gz bullseyeWMparc_MNIspace_1mm.nii.gz
fi


((count++));
echo $count

done


