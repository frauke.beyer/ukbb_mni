# Create map to differentiate WM at > 10mm distance from ventricles

## Steps to take to create a distance map which can then be used for binarizing
FSL --version 5.0.11

`fast /data/pt_life_whm/Data/segmented_MNI1mm/MNI152_T1_1mm_brain.nii.gz`

`fslmaths /data/pt_life_whm/Data/segmented_MNI1mm/MNI152_T1_1mm_brain_pve_0.nii.gz -thr 0.9 -bin /data/pt_life_whm/Data/segmented_MNI1mm/MNI152_T1_1mm_brain_pve_0_thr0.9_bin.nii.gz`

`distancemap -i /data/pt_life_whm/Data/segmented_MNI1mm/MNI152_T1_1mm_brain_pve_0_thr0.9_bin.nii.gz -o /data/pt_life_whm/Data/segmented_MNI1mm/MNI152_T1_1mm_brain_pve_0_thr0.9_bin_distancemap.nii.gz`

`fslmaths dist_to_vent -uthr <dist> -bin dist_to_vent_periventricular`
`fslmaths dist_to_vent -thr <dist> -bin dist_to_vent_deep`

