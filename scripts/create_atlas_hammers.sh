#!/bin/bash
cd /data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers_based/individual_regions_Hammers/Hammers_edited/

ROIdir="/data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers_based/Hammers/"
distdir="/data/pt_life_whm/Analysis/ukbb_mni/WMparcellations_MNI/Hammers_based/distance_maps/Hammers_edited/"
for roi in frontal_lh frontal_rh temporal_lh temporal_rh occipital_lh occipital_rh parietal_lh parietal_rh BG
do
fslmaths ${ROIdir}/Hammers_${roi}2MNI.nii.gz -mas ${distdir}/rel_distancemap_ventricle_thr0.5_cortex_0_0.25.nii.gz Hammers_${roi}_1shell.nii
fslmaths ${ROIdir}/Hammers_${roi}2MNI.nii.gz -mas ${distdir}/rel_distancemap_ventricle_thr0.5_cortex_0.25_0.5.nii.gz Hammers_${roi}_2shell.nii
fslmaths ${ROIdir}/Hammers_${roi}2MNI.nii.gz -mas ${distdir}/rel_distancemap_ventricle_thr0.5_cortex_0.5_0.75.nii.gz Hammers_${roi}_3shell.nii
fslmaths ${ROIdir}/Hammers_${roi}2MNI.nii.gz -mas ${distdir}/rel_distancemap_ventricle_thr0.5_cortex_0.75_1.nii.gz Hammers_${roi}_4shell.nii

#fslmaths ${ROIdir}/Hammers_${roi}2MNI.nii.gz -mas ${distdir}/rel_distancemap_ventricle_thr0.5_cortex_0_0.33.nii.gz Hammers_${roi}_1shell_3.nii
#fslmaths ${ROIdir}/Hammers_${roi}2MNI.nii.gz -mas ${distdir}/rel_distancemap_ventricle_thr0.5_cortex_0.33_0.66.nii.gz Hammers_${roi}_2shell_3.nii
#fslmaths ${ROIdir}/Hammers_${roi}2MNI.nii.gz -mas ${distdir}/rel_distancemap_ventricle_thr0.5_cortex_0.66_1.nii.gz Hammers_${roi}_3shell_3.nii


done


# Create atlas file (1-34) ##without occipital 1
count=1

for file in `ls Hammers*shell.nii.gz`
do

#ONLY when doing Hammers without 1st and 2nd occipital shell
#if [ "$file" == "Hammers_occipital_lh_1shell.nii.gz" ] || [ "$file" == "Hammers_occipital_rh_1shell.nii.gz" ] || [ "$file" == "Hammers_occipital_lh_2shell.nii.gz" ] || [ "$file" == "Hammers_occipital_rh_2shell.nii.gz" ];
#then
#echo $file
#echo "lh/rh first shell"

#continue


#else
echo $file  >> order.txt
#echo "not occipital"
echo $count

if [ $count == 1 ];
then
echo "creating first tmp"
fslmaths $file -mul $count bullseyeWMparc_Hammers_edited_MNIspace_36ROI_1mm.nii.gz
((count++));
echo $count
else
echo "adding other volumes first tmp"
fslmaths $file -mul $count -add bullseyeWMparc_Hammers_edited_MNIspace_36ROI_1mm.nii.gz bullseyeWMparc_Hammers_edited_MNIspace_36ROI_1mm.nii.gz
((count++));
echo $count
fi

#fi



done


