SUBJECTS_DIR="/data/pt_life_whm/Data/segmented_MNI1mm"
mkdir $SUBJECTS_DIR/fsaverage/labels_new

for hemi in lh rh
do
mkdir /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.lobes/

mri_annotation2label --subject fsaverage --hemi $hemi --annotation aparc --outdir $SUBJECTS_DIR/fsaverage/labels_new


 #Frontal
# • Superior Frontal
# • Rostral and Caudal Middle Frontal
# • Pars Opercularis, Pars Triangularis, and Pars Orbitalis
# • Lateral and Medial Orbitofrontal
# • Precentral
# • Paracentral
# • Frontal Pole
# • Rostral Anterior Cingulate
# • Caudal Anterior Cingulate


mri_mergelabels -i /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.caudalmiddlefrontal.label -i /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.superiorfrontal.label -i /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.rostralmiddlefrontal.label -i /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.rostralanteriorcingulate.label -i /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.frontalpole.label -i /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.precentral.label -i /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.parstriangularis.label -i /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.parsorbitalis.label -i /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.parsopercularis.label -i /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.paracentral.label -i /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.medialorbitofrontal.label -i /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.lateralorbitofrontal.label -i /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.caudalanteriorcingulate.label -o /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.lobes/$hemi.frontal

#> Parietal
#> • Superior Parietal
#> • Inferior Parietal
#> • Supramarginal
#> • Postcentral
#> • Precuneus
#> • Posterior Cingulate
#> • Isthmus

mri_mergelabels -i /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.superiorparietal.label -i /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.inferiorparietal.label -i /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.postcentral.label -i /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.precuneus.label -i /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.posteriorcingulate.label -i /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.isthmuscingulate.label -o /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.lobes/$hemi.parietal

#> Temporal
#> • Superior, Middle, and Inferior Temporal
#> • Banks of the Superior Temporal Sulcus
#> • Fusiform
#> • Transverse Temporal
#> • Ento$hemiinal
#> • Temporal Pole
#> • Parahippocampal

mri_mergelabels -i /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.inferiortemporal.label -i /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.middletemporal.label -i /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.entorhinal.label -i /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.parahippocampal.label -i /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.superiortemporal.label -i /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.transversetemporal.label -i /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.fusiform.label -o /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.lobes/$hemi.temporal

#> Occipital
#> • Lateral Occipital
#> • Lingual
#> • Cuneus
#> • Pericalcarine

mri_mergelabels -i /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.cuneus.label -i /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.lingual.label -i /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.pericalcarine.label -i /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.lateraloccipital.label -o /data/pt_life_whm/Data/segmented_MNI1mm/fsaverage/labels_new/$hemi.lobes/$hemi.occipital

done
