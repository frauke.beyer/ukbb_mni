# Templates for Bullseye WM segmentation in MNI space

## scripts
- scripts to generate Bullseye WM segmentation in MNI space from 
	- fsaverage (`README_create_bullseye_MNI_atlas.md`)
	- Hammers atlas (`README_create_bullseye_MNI_atlas_Hammers.md`)
	
## bullseye_MNI_Hammers_edited
- final template using Hammers atlas + edited ventricles in occipital lobe
	- `bullseyeWMparc_Hammers_edited_MNIspace_36ROI_1mm.nii.gz`: to be used for WMH segmentation
	- `lookuptable_bullseye_Hammers_edited_36ROI_MNIatlas.txt`: lookup table for WMH segmentation


These two previous templates were not optimal:	

## bullseye_MNI_Hammers
- first template using Hammers atlas: `bullseyeWMparc_Hammers_MNIspace_1mm.nii.gz` & lookuptable
(here, no voxel were in occipital shell 1 because of ventricle segmentation in occipital lobe)
- template without occipital shell 1: `bullseyeWMparc_Hammers_MNIspace_34ROI_1mm.nii.gz` & lookuptable
- template with only three shells: `bullseyeWMparc_Hammers_MNIspace_3shells_1mm.nii.gz` & lookuptable

## bullseye_MNI_fsaverage/
- template using fsaverage: `bullseye_MNIatlas_1mm.nii.gz` & lookuptable
(here, ventricle segmentation from fsaverage was brought to MNI template, leading to very large ventricles and exclusion of WM voxels in periventricular zone)
